## log-test

A simple app that allows for testing of four different JS logging frameworks: PnP, SpFx, Bunyan, and LogLevel.

### Building and running the code

```
npm install
gulp serve
```