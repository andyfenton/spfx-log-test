declare interface ILogTestingWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'LogTestingWebPartStrings' {
  const strings: ILogTestingWebPartStrings;
  export = strings;
}
