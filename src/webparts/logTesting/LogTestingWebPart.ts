import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';

import * as strings from 'LogTestingWebPartStrings';
import LogTesting from './components/LogTesting';
import { LogTestingProps } from './components/LogTestingProps';

export interface ILogTestingWebPartProps {
  description: string;
}

export default class LogTestingWebPart extends BaseClientSideWebPart<ILogTestingWebPartProps> {

  public render(): void {
    const element: React.ReactElement<LogTestingProps > = React.createElement(
      LogTesting,
      {
        description: this.properties.description
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
