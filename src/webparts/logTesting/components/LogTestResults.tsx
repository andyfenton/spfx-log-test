import * as React from 'react';
import styles from './LogTesting.module.scss';

import * as _ from 'lodash';

import { Log } from '@microsoft/sp-core-library';

import * as BunyanLogger from '../../../Loggers/BunyanLogger';
import * as LogLevelLogger from '../../../Loggers/LogLevelLogger';
import ConsoleLogHandler, {LogLevel} from '../../../Loggers/SPFxLogger';
import * as PnPLogger from '../../../Loggers/PnPLogger';

import {LogResultsProps} from './LogTestingProps';
import {LogResultsStates} from './LogTestingStates';

export default class LogtestResults extends React.Component<LogResultsProps, LogResultsStates> {
    spFxLogger: ConsoleLogHandler;

    state = {
        testCount: "0",
        testTime: "0"
    }

    constructor(props) {
       super(props);
       
        this.spFxLogger = new ConsoleLogHandler(LogLevel.Info);
        Log._initialize(this.spFxLogger);
    }

    log = (data: string): void => {   
        switch(this.props.logName) {
            case "Bunyan":
                BunyanLogger.warn(data);
                break;
            case "LogLevel":
                LogLevelLogger.warn(data);
                break;
            case "SpFx":
                Log.warn("LogTestResults", data, undefined);
                break;
            case "PnP":
                PnPLogger.warn("LogTestResults");
                break;
        }

    }

    startTest = (event) => {
        event.preventDefault();

        let start: number = new Date().getTime();

        _.range(0, this.props.logCount).forEach((current, index, range) => {
            this.log(index.toString());
        });

        let time: number = (Math.abs(new Date().getTime() - start)) * .001;

        this.setState({testTime: time.toFixed(3).toString()});
        this.setState({testCount: this.props.logCount.toString()});
    }

    public render() {
        return (
            <form onSubmit={this.startTest}>
                <table className={styles.column}><tbody>
                    <tr className={styles.subTitle}><td>{this.props.logName}</td></tr>
                    <tr><td><input className={styles.button} type="submit" value="Start Test" /></td></tr>
                    <tr className={styles.subTitle}><td>{this.state.testCount}</td></tr>
                    <tr className={styles.subTitle}><td>{this.state.testTime}</td></tr>
                </tbody></table>
            </form>
        );
    }
}