import * as React from 'react';
import styles from './LogTesting.module.scss';
import { LogTestingProps } from './LogTestingProps';
import { LogTestingStates } from './LogTestingStates';
import { escape } from '@microsoft/sp-lodash-subset';

import LogForm from './LogForm';
import LogTestResults from './LogTestResults';

export default class LogTesting extends React.Component<LogTestingProps, LogTestingStates> {
  state = {
    logCount: 100
  }

  logCountChanged = (count: number): void => {
    this.setState({logCount: count});
  }

  public render(): React.ReactElement<LogTestingProps> {
    return (
      <div className={ styles.logTesting }>
        <div className={ styles.container }>
          <div className={ styles.row }>
            <div className={ styles.column }>
              <span className={ styles.title }>SharePoint FX Log Testing</span>
              <LogForm logCountChanged={this.logCountChanged}/>
            </div>
          </div>
          <table className={styles.table}><tbody>
            <tr>
              <td><LogTestResults logName="PnP" logCount={this.state.logCount}/></td>
              <td><LogTestResults logName="Bunyan" logCount={this.state.logCount}/></td>
              <td><LogTestResults logName="SpFx" logCount={this.state.logCount}/></td>
              <td><LogTestResults logName="LogLevel" logCount={this.state.logCount}/></td>
            </tr>
          </tbody></table>
        </div>
      </div>
    );
  }
}
