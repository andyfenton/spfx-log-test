export interface LogTestingStates {
    logCount: number;
}

export interface LogFormStates {
    logCount: string;
}

export interface LogResultsStates {
    testCount: string;
    testTime: string;
}