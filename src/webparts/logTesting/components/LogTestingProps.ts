export interface LogTestingProps {
  description: string;
}

export interface LogFormProps {
  logCountChanged(count: number): void;
}

export interface LogResultsProps {
  logName: string;
  logCount: number;
}