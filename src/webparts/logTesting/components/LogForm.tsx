import * as React from 'react';
import styles from './LogTesting.module.scss';

import {LogFormProps} from './LogTestingProps';
import {LogFormStates} from './LogTestingStates';

export default class LogForm extends React.Component<LogFormProps, LogFormStates> {
    state = {
        logCount: "100"
    };

    handleReset = (event) => {
        console.log("Reset");
        this.setState({logCount: "0"});
        this.props.logCountChanged(0);
    };

    handleCountChange = (event) => {
        let value: string = event.target.value.replace(/\D/, '');
        this.setState({ logCount: value });
        this.props.logCountChanged(+value);
    };

    public render() {
        return (
            <form onReset={ this.handleReset }>
                <p>
                    <input className={ styles.textBox } type="text"
                        value={this.state.logCount}
                        onChange={this.handleCountChange}
                        required />
                </p>
            </form>
        );
    }
}