import * as log from 'loglevel';

log.setDefaultLevel(log.levels.INFO);

function getDateTime(): string {
    let date = new Date();
    let returnString = `${date.getMonth() + 1}\\${date.getDate()}\\${date.getFullYear()}`;
    returnString += `-${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.${date.getMilliseconds()}`;
    return returnString;
}

export function info(data: string): void {
    log.info("[%s] INFO: %s", getDateTime(), data);
}

export function warn(data: string): void {
    log.warn("[%s] WARN: %s", getDateTime(), data);
}

export function error(data: string, error?: Error): void {
    let errorString = data;
    if (error) {
        console.log("We have an error");
        errorString += " -- " + Error.toString();
    }
    log.error("[%s] ERROR: %s", getDateTime(), errorString);
}