import * as bunyan from 'bunyan';

const bunyanLog = bunyan.createLogger({name: 'TestLogging'})

export function info(data: string) {
    bunyanLog.info(data);
}

export function warn(data: string) {
    bunyanLog.warn(data);
}

export function error(data: string) {
    bunyanLog.error(data);
}