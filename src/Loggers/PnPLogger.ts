import {
    Logger,
    ConsoleListener,
    LogLevel
} from "sp-pnp-js";

function getDateTime(): string {
    let date = new Date();
    let returnString = `${date.getMonth() + 1}\\${date.getDate()}\\${date.getFullYear()}`;
    returnString += `-${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.${date.getMilliseconds()}`;
    return returnString;
}

export function info(data: string): void {
    let message: string = `${getDateTime()} INFO: ${data}`;
    Logger.write(message, LogLevel.Info);
}

export function warn(data: string): void {
    let message: string = `${getDateTime()} WARN: ${data}`;
    Logger.write(message, LogLevel.Warning);
}

export function error(data: string, error?: Error): void {
    let message: string = `${getDateTime()} ERROR: ${data}`;
    Logger.write(message, LogLevel.Error);
}

Logger.subscribe(new ConsoleListener());